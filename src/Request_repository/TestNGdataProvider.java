package Request_repository;

import org.testng.annotations.DataProvider;

public class TestNGdataProvider {
	@DataProvider()
	public Object[][] post_data_provider(){
		return new Object[][]
				{
					{"morpheus","leader"},
					{"balaji","QA"},
					{"akshay","TL"}
				};
	}
}
