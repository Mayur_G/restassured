package Test_Case_Packg;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.AssertJUnit;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import API_common_Methods.Common_Method_Handle_API;
import Endpoint.Post_endpoint;
import Request_repository.Post_request_repository;
import Request_repository.TestNGdataProvider;
import io.restassured.path.json.JsonPath;
import utility_common_methods.Handle_API_logs;
import utility_common_methods.Handle_directory;

public class TestDataValidation extends Common_Method_Handle_API{
	static File logDir;
	static String request_body;
	static String end_point;
	static String response_body;

	@DataProvider()
	public Object[][] post_requestBody(){
		return new Object[][]
				{
					{"morpheus","leader"},
					{"Nikhil","QA"},
					{"Shrinath","React"}
				};
	}
	
	@BeforeTest
	public static void test_setup() throws IOException {
		logDir = Handle_directory.Create_log_directory("Post_TestCase.logs");
		end_point = Post_endpoint.Post_endpoint_TC1();
	}

	@Test(dataProvider = "post_requestBody")
	public static void post_executor(String name, String job) throws IOException {
		request_body = "{\r\n"
				+ "    \"name\": \""+name+"\",\r\n"
				+ "    \"job\": \""+job+"\"\r\n"
				+ "}";
		for (int i = 0; i < 5; i++) {
			int status_code = Post_statusCode(end_point, request_body);
			System.out.println(status_code);
			if (status_code == 201) {
				String response_body = Post_responsebody(end_point, request_body);
				System.out.println(response_body);
				TestDataValidation.validator(request_body, response_body);
				break;
			} else {
				System.out.println("expected status code is not found hence retrying");
			}
		}
	}

	public static void validator(String requestBody, String responseBody) {
//		parse the request body and response body
		JsonPath JPReq = new JsonPath(requestBody);
		String reqName = JPReq.getString("name");
		String reqJob = JPReq.getString("job");
		LocalDateTime curr_date = LocalDateTime.now();
		String exp_date = curr_date.toString().substring(0, 11);

		JsonPath JPRes = new JsonPath(responseBody);
		int resId = JPRes.getInt("id");
		String resName = JPRes.getString("name");
		String resJob = JPRes.getString("job");
		String resDate = JPRes.getString("createdAt").substring(0, 11);

//		validate the response body
		Assert.assertNotNull(resId);
		Assert.assertEquals(reqName, resName);
		Assert.assertEquals(reqJob, resJob);
		Assert.assertEquals(exp_date, resDate);
	}

	@AfterTest
	public void test_teardown() throws IOException {
		String Post_Request_Log_File = this.getClass().getName();
		Handle_API_logs.evidence_creator(logDir, Post_Request_Log_File, request_body, end_point, response_body);
	}
}
