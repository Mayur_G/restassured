package Test_Case_Packg;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import java.io.File;
import java.io.IOException;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import API_common_Methods.Common_Method_Handle_API;
import Endpoint.Get_endpoint;
import Endpoint.Post_endpoint;
import Request_repository.Post_request_repository;
import io.restassured.path.json.JsonPath;
import utility_common_methods.Handle_API_logs;
import utility_common_methods.Handle_directory;

public class Get_TestCase extends Common_Method_Handle_API {
	static File log_dir;
	static String endpoint;
	static String responsebody;
	
	@BeforeTest
	public static void Test_setup() throws IOException {
		 log_dir=Handle_directory.Create_log_directory("Get_TestCase.logs");
		 endpoint = Get_endpoint.Get_endpoint_TC3();
	}
	
	@Test
	public static void get_executor() throws IOException {
		
		for (int i = 0; i < 5; i++) {
			int statuscode = get_statusCode(endpoint);
			System.out.println(statuscode);
			if (statuscode == 200) {
				String responsebody = get_responsebody(endpoint);
				System.out.println(responsebody);
      			Get_TestCase.validator(responsebody);
				break;
			}

		}
	}

	public static void validator(String responsebody) {
		int expected_id[] = { 7, 8, 9, 10, 11, 12 };
		String expected_firstname[] = { "Michael", "Lindsay", "Tobias", "Byron", "George", "Rachel" };
		String expected_lastname[] = { "Lawson", "Ferguson", "Funke", "Fields", "Edwards", "Howell" };
		String expected_email[] = { "michael.lawson@reqres.in", "lindsay.ferguson@reqres.in", "tobias.funke@reqres.in",
				"byron.fields@reqres.in", "george.edwards@reqres.in", "rachel.howell@reqres.in" };
		JsonPath jsp_res = new JsonPath(responsebody);
		JSONObject array_res = new JSONObject(responsebody);
		JSONArray dataarray = array_res.getJSONArray("data");
		System.out.println(dataarray);
		int count = dataarray.length();
		System.out.println(count);
		for (int i = 0; i < count; i++) {
			int res_id = dataarray.getJSONObject(i).getInt("id");
			System.out.println(res_id);
			int exp_id = expected_id[i];
			String res_firstname = dataarray.getJSONObject(i).getString("first_name");
			String exp_firstname = expected_firstname[i];
			String res_lastname = dataarray.getJSONObject(i).getString("last_name");
			String exp_lastname = expected_lastname[i];
			String res_email = dataarray.getJSONObject(i).getString("email");
			String exp_email = expected_email[i];

			AssertJUnit.assertEquals(res_id, exp_id);
			AssertJUnit.assertEquals(res_firstname, exp_firstname);
			AssertJUnit.assertEquals(res_lastname, exp_lastname);
			AssertJUnit.assertEquals(res_email, exp_email);

		}
	}
	@AfterTest
	public static void Teardown() throws IOException {
		Handle_API_logs.evidence_creator(log_dir,"Get_TestCase" ,  endpoint,  null, responsebody);

	}
}
